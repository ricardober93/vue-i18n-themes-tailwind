import routesTodos from '@/modules/todo/router'
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/idiomas',
    name: 'idiomas',
    component: () => import(/* webpackChunkName: "idiomas" */ '../views/Idiomas.vue')
  },
  ...routesTodos
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
