import { RouteRecordRaw } from 'vue-router'
import Todo from '@/modules/todo/Todo.vue'

const routesTodos: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'todo',
    component: Todo,
    children: [
      {
        path: '/todo',
        name: 'composition',
        component: () => import(/* webpackChunkName: "about" */ '../views/Composition.vue')
      },
      {
        path: '/options',
        name: 'options',
        component: () => import(/* webpackChunkName: "about" */ '../views/Options.vue')
      }
    ]
  }
]

export default routesTodos
